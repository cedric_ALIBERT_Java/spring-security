<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Company home page</title>
</head>
<body>
	<h2>Company home page</h2>
	<hr>
	<p>
		Welcome to home page !
	</p>
	
	<hr>
	<p>
		User: <security:authentication property="principal.username"/>
		<br><br>
		Role(s): <security:authentication property="principal.authorities"/>
	</p>
	
	<hr>
	<p>
		<security:authorize access="hasRole('MANAGER')">
			<a href="${pageContext.request.contextPath}/leaders">Leadership Meeting</a>
			(Only for manager people)
		</security:authorize>
		
		<security:authorize access="hasRole('ADMIN')">
			<a href="${pageContext.request.contextPath}/systems">Admin Meeting</a>
			(Only for admin people)
		</security:authorize>
	</p>
	<hr>
	
	<form:form action="${pageContext.request.contextPath}/logout" method="POST">
		<input type="submit" value="Logout"/>
	</form:form>
</body>
</html>